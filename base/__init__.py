import os
from pyrogram import Client
from dotenv import load_dotenv
load_dotenv()

ADMINS = os.getenv('ADMINS')

plugins = dict(root="base/plugins")

Null_Bot = Client(
    "Null-Bot",
    api_id=os.getenv('API_ID'),
    api_hash=os.getenv('API_HASH'),
    bot_token=os.getenv('TOKEN'),
    workers=int(os.getenv('WORKERS')),
    plugins=plugins
)
